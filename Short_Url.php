<?php
class Short_Url
{
	var $host;
	var $user;
	var $pass;
	var $database; 
	var $base_url; 
	function __construct()
	{ 
		$this->host = 'localhost';
		$this->user = 'root';
		$this->pass = '';
		$this->database = 'infama';  
		$this->base_url = 'http://localhost/test/';  
	}
 
    function connect()
	{
		$conn = new mysqli($this->host, $this->user, $this->pass, $this->database);
		if ($conn->connect_error) 
		{
			die("Connection failed: " . $conn->connect_error);
		}
		return $conn;
	}

	function get_short_url($long_url='')
	{
		
			$result='';
			$conn = $this->connect();
			$result = $conn->query("SELECT * FROM short_url_tb WHERE url = '".$long_url."' ");
			if($result->num_rows > 0) 
			{
				$row = $result->fetch_assoc();
				$result = $row['short_url']; 
			}
			else 
			{
				$short_url = $this->getUniqueCode();
				$sql = "INSERT INTO short_url_tb (url, short_url, requests)VALUES ('".$long_url."', '".$short_url."', '0')";
				if ($conn->query($sql) === TRUE) 
				{
					$result = $short_url;
				} 
				else { 
					 die("Unknown Error Occured");
				}
			} 
		return $this->base_url."?r=".$result;
	}
	
	function get_redirect_url($url='')
	{ 
		$result = '';
		$conn = $this->connect();
		$query = $conn->query("SELECT * FROM short_url_tb WHERE short_url = '".addslashes($url)."' ");
		if ($query->num_rows > 0) 
		{
			$row = $query->fetch_assoc();  
			$result = $row['url'];
			//Update requests counter
			$conn->query("UPDATE short_url_tb SET requests=requests+1 WHERE id='".$row['id']."' ");
			//
		}
		else 
		 { 
			return "Invalid";
		}
		return $result;
	}
	
	function getUniqueCode()
	{
		$conn = $this->connect();
		$token = substr(md5(uniqid(rand(), true)),0,6);  
		$result = $conn->query("SELECT * FROM short_url_tb WHERE short_url = '".$token."' "); 
		if ($result->num_rows > 0) 
		{
			$this->getUniqueCode();
		}
		else 
		{
			return $token;
		}
	}
}
?>