<?php

    //SIMPLE CODE TO SHORTEN LONG URL
	//Author: Langat Kipkoech
	//Email: kipkoechlangat69@gmail.com
	//Date : 24-03-2021
	
	//TASK 1
       require_once('Short_Url.php');
	   $short_url_obj=new Short_Url();
		
		//get Short URL
        if(isset($_GET['url']))
		{ 
			$url=urldecode($_GET['url']);
			if (filter_var($url, FILTER_VALIDATE_URL)) 
			{
				$short_url=$short_url_obj->get_short_url($url);
				echo json_encode(array('status'=>'success','short_url'=>$short_url));
			}else
			{
				echo json_encode(array('status'=>'failed','message'=>'Invalid URL'));
			}
		}
		
		//Redirects
		if(isset($_GET['r']))
		{
			$url=urldecode($_GET['r']);
			$new_url=$short_url_obj->get_redirect_url($url); 
			if($new_url == 'Invalid')
			{
				echo json_encode(array('status'=>'failed','message'=>'Invalid Link!'));
			}
			else
			{
				//redirect to this URL $new_url
				//header("location:".$new_url);
				echo json_encode(array('status'=>'success','url'=>$new_url));
			}			
				  
		}
	//END OF TASK 1
     //TASK 2	
		
		function movies($lengthofFlight,$movieDuration=array())
		{
			$result= false;
			for($i=0; $i < count($movieDuration)-1; $i++)
			{
				for($j=1; $j < count($movieDuration); $j++)
				{
					//add the duration for 2 movies and compare with the length of Flight where the 2 movies are not the same
					if( (($movieDuration[$i] + $movieDuration[$j]) == $lengthofFlight ) && ($i != $j) )
					{
						$result = true;
					}
				}
			}
			return $result;
		}
	//END OF TASK 2
?>